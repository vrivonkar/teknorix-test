﻿using TeknorixTest.Models;
using System.Threading.Tasks;

namespace TeknorixTest.Services.Authentication
{
    public interface ILocalAuthenticationService
    {
        Task SignIn(UserLoginModel userLogin, bool rememberMe);
        Task SignOut();
    }
}