﻿using TeknorixTest.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace TeknorixTest.Services.Authentication
{
    public class CookieAuthenticationService : ILocalAuthenticationService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public CookieAuthenticationService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;

        }

        public async Task SignIn(UserLoginModel userLogin, bool rememberMe)
        {

            var claimsIdentity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, userLogin.Email),
             }, "Cookies");

            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
            await _httpContextAccessor.HttpContext.SignInAsync("Cookies", claimsPrincipal);
            var cookiedata = _httpContextAccessor.HttpContext.Response.Headers["Set-Cookie"].ToString();

            var cookie = cookiedata.Split(";")[0].Replace("yacth_cookie=", string.Empty);
            _httpContextAccessor.HttpContext.Response.Cookies.Append("inv_cookie", cookie);
        }

        public async Task SignOut()
        {
            await _httpContextAccessor.HttpContext.SignOutAsync("Cookies");
        }
    }
}
