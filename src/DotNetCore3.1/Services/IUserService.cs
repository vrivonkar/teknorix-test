﻿using TeknorixTest.Models;

namespace TeknorixTest.Services
{
    public interface IUserService
    {
        bool ValidateUser(UserLoginModel model);
    }
}