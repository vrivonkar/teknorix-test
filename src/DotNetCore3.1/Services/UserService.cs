﻿using TeknorixTest.Data.Domain;
using TeknorixTest.Data.Infrastructure.Domain;
using TeknorixTest.Models;
using System.Linq;

namespace TeknorixTest.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _userRepository;


        public UserService(IRepository<User> userRepository)
        {
            _userRepository = userRepository;

        }

        public bool ValidateUser(UserLoginModel model)
        {
            var getuser = _userRepository.GetAll()
                                        .Where(r => r.Email.ToLower() == model.Email.ToLower())
                                        .FirstOrDefault();
            if (getuser != null)
            {
                return getuser.Password == model.Password;
            }
            return false;
        }
    }
}
