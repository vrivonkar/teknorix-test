﻿using TeknorixTest.Models;
using TeknorixTest.Services;
using TeknorixTest.Services.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace TeknorixTest.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService _userService;
        private readonly ILocalAuthenticationService _authenticationService;

        public AccountController(IUserService userService,
            ILocalAuthenticationService authenticationService)
        {
            _userService = userService;
            _authenticationService = authenticationService;

        }
        [AllowAnonymous]
        public IActionResult Login(string returnUrl)
        {

            var model = new UserLoginModel { ReturnUrl = returnUrl };
            return View(model);
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult Login(UserLoginModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = _userService.ValidateUser(model);
            if (result)
            {
                return RedirectToLocal(returnUrl);
            }
            ModelState.AddModelError("", "Invalid login attempt.");
            return View(model);

        }
        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }
    }
}
