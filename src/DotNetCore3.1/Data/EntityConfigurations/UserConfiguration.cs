﻿using TeknorixTest.Data.Domain;
using TeknorixTest.Data.Infrastructure.EntityConfigurations;

namespace TeknorixTest.Data.EntityConfigurations
{
    public class UserConfiguration : BaseConfiguration<User>
    {

        public override void SetPrimaryKey()
        {
            Builder.HasKey(r => r.Id);
        }
        public override void SetProperties()
        {
            Builder.Property(r => r.Id).IsRequired().ValueGeneratedOnAdd();
            Builder.Property(r => r.Email).HasMaxLength(500).IsRequired();
            Builder.Property(r => r.FirstName).HasMaxLength(500).IsRequired();
            Builder.Property(r => r.IsActive).IsRequired();
        }
        public override void SetForeignKey()
        {
            base.SetForeignKey();
        }
    }
}
