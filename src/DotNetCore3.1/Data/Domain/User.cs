﻿using TeknorixTest.Data.Infrastructure.Domain.Entities;
using System;

namespace TeknorixTest.Data.Domain
{
    public class User : BaseEntity
    {
        public User()
        {
            CreatedAt = DateTime.Now;
            IsActive = true;
        }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsActive { get; set; }
    }
}
