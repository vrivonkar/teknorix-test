﻿
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace TeknorixTest.Data.Infrastructure.Domain
{

    public class GenericRepository<TEntity> : GenericRepository<TEntity, int>, IRepository<TEntity>
        where TEntity : class, IEntity<int>
    {
        public GenericRepository(IDbContextProvider dbContextProvider) : base(dbContextProvider)
        {

        }


    }

    public class GenericRepository<TEntity, TPrimaryKey> : IRepository<TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {

        protected IDbContextProvider DbContextProvider { get; }

        public DbContext Context => DbContextProvider.GetDbContext();

        public DbSet<TEntity> Table { get; set; }

        //public DbQuery<TEntity> Querable { get; set; }

        public GenericRepository(IDbContextProvider dbContextProvider)
        {
            DbContextProvider = dbContextProvider;

            Table = Context.Set<TEntity>();
            var types = Context.Model.GetEntityTypes();
            if (types.Any())
            {
                var entityType = types.Where(t => t.ClrType == typeof(TEntity)).FirstOrDefault();
                /* if (entityType != null && entityType.IsQueryType)
                 {
                     Querable = Context.Query<TEntity>();
                 }
                 else
                 {*/

                // }
            }
        }

        #region DbQuery

        #endregion

        #region Select/Get/Query

        public virtual IQueryable<TEntity> GetAll(List<Expression<Func<TEntity, object>>> includes = null)
        {
            return GetAllIncluding(includes);
        }

        public virtual IQueryable<TEntity> GetAllWithTracking(List<Expression<Func<TEntity, object>>> includes = null)
        {
            return GetAllIncludingWithTracking(includes);
        }

        public virtual IQueryable<TEntity> GetAll(string sql, params object[] parameters)
        {
            return Table.FromSqlRaw(sql, parameters).AsNoTracking();// Querable.FromSqlInterpolated(sql, parameters);
        }

        public virtual IQueryable<TEntity> GetAllIncluding(List<Expression<Func<TEntity, object>>> propertySelectors)
        {

            var query = Table != null ? Table.AsNoTracking().AsQueryable() : null;// Querable.AsQueryable();

            if (propertySelectors?.Any() == true)
            {
                propertySelectors.ForEach(p => query = query.Include(p));
            }

            return query;
        }

        public virtual IQueryable<TEntity> GetAllIncludingWithTracking(List<Expression<Func<TEntity, object>>> propertySelectors)
        {

            var query = Table != null ? Table.AsQueryable() : null;// Querable.AsQueryable();

            if (propertySelectors?.Any() == true)
            {
                propertySelectors.ForEach(p => query = query.Include(p));
            }

            return query;
        }

        public virtual IQueryable<TEntity> GetAllIncluding(string sql, List<Expression<Func<TEntity, object>>> includes, params object[] parameters)
        {
            var query = GetAll(sql, parameters);

            if (includes?.Any() == true)
            {
                foreach (var prop in includes)
                {
                    query = query.Include(prop);
                }
            }

            return query;
        }

        public virtual Task<List<TEntity>> GetAllListAsync(string sql, params object[] parameters)
        {
            var query = GetAll(sql, parameters);
            return query.ToListAsync();
        }

        public virtual Task<List<TEntity>> GetAllListAsync(string sql, List<Expression<Func<TEntity, object>>> includes = null, params object[] parameters)
        {
            var query = GetAllIncluding(sql, includes, parameters);
            return query.AsNoTracking().ToListAsync();
        }

        public virtual List<TEntity> GetAllList()
        {
            return GetAll().ToList();
        }

        public virtual Task<List<TEntity>> GetAllListAsync()
        {
            return GetAll().ToListAsync();
        }

        public virtual List<TEntity> GetAllList(Expression<Func<TEntity, bool>> predicate)
        {
            var query = GetAll();

            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            return query.ToList();
        }

        public virtual T Query<T>(Func<IQueryable<TEntity>, T> queryMethod)
        {
            return queryMethod(GetAll());
        }

        public virtual TEntity Get(TPrimaryKey id)
        {
            return FirstOrDefault(id);

        }

        public virtual async Task<TEntity> GetAsync(TPrimaryKey id, List<Expression<Func<TEntity, object>>> includes = null)
        {
            var entity = await FirstOrDefaultAsync(id, includes);

            return entity;
        }

        public virtual TEntity Single(Expression<Func<TEntity, bool>> predicate)
        {
            return GetAllWithTracking().Single(predicate);
        }

        public virtual Task<TEntity> SingleAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return GetAllWithTracking().SingleAsync(predicate);
        }

        public virtual TEntity FirstOrDefault(TPrimaryKey id)
        {
            return GetAllWithTracking().FirstOrDefault(CreateEqualityExpressionForId(id));
        }

        public virtual Task<TEntity> FirstOrDefaultAsync(TPrimaryKey id, List<Expression<Func<TEntity, object>>> includes = null)
        {
            return GetAll(includes).FirstOrDefaultAsync(CreateEqualityExpressionForId(id));
        }

        public virtual TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate, List<Expression<Func<TEntity, object>>> includes = null)
        {
            return GetAll().FirstOrDefault(predicate);
        }

        public virtual Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate, List<Expression<Func<TEntity, object>>> includes = null)
        {
            return GetAllWithTracking(includes).FirstOrDefaultAsync(predicate);
        }

        public virtual TEntity Load(TPrimaryKey id)
        {
            return Get(id);
        }

        #endregion

        #region Insert

        public virtual TEntity Insert(TEntity entity)
        {
            //Table.Add(entity);
            //Context.SaveChanges();
            //return entity;
            //saved when SaveChanges is called on the datacontext to maintain consistency
            return Table.Add(entity).Entity;
        }

        public virtual Task<TEntity> InsertAsync(TEntity entity)
        {
            return Task.FromResult(Insert(entity));
        }

        public virtual TPrimaryKey InsertAndGetId(TEntity entity)
        {
            entity = Insert(entity);
            if (IsTransient(entity)) //this condition it not met when running on in memory database
            {
                Context.SaveChanges();
            }
            return entity.Id;
        }

        public virtual async Task<TPrimaryKey> InsertAndGetIdAsync(TEntity entity)
        {
            entity = await InsertAsync(entity);
            if (IsTransient(entity))
            {
                Context.SaveChanges();
            }
            return entity.Id;
        }

        public virtual TEntity InsertOrUpdate(TEntity entity)
        {
            return IsTransient(entity) ? Insert(entity) : Update(entity);
        }

        public virtual Task<TEntity> InsertOrUpdateAsync(TEntity entity)
        {
            return IsTransient(entity) ? InsertAsync(entity) : UpdateAsync(entity);
        }

        public virtual TPrimaryKey InsertOrUpdateAndGetId(TEntity entity)
        {
            entity = InsertOrUpdate(entity);
            return entity.Id;
        }

        public virtual async Task<TPrimaryKey> InsertOrUpdateAndGetIdAsync(TEntity entity)
        {
            entity = await InsertOrUpdateAsync(entity);
            return entity.Id;
        }

        #endregion

        #region Update


        public virtual TEntity Update(TEntity entity)
        {
            AttachIfNot(entity);
            Context.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public virtual Task<TEntity> UpdateAsync(TEntity entity)
        {
            return Task.FromResult(Update(entity));
        }

        public virtual Task UpdateAsync(Expression<Func<TEntity, bool>> predicate, string[] properties, object[] values)
        {
            Update(predicate, properties, values);
            return Task.FromResult(0);
        }

        public virtual void Update(Expression<Func<TEntity, bool>> predicate, string[] properties, object[] values)
        {
            foreach (var entity in GetAll().Where(predicate).ToList())
            {

                for (int i = 0; i < properties.Count(); i++)
                {
                    if (entity.GetType().GetProperty(properties.ElementAt(i)) != null)
                    {
                        entity.GetType().GetProperty(properties.ElementAt(i)).SetValue(entity, values.ElementAt(i));
                    }
                    Update(entity);
                }



            }
        }

        // TODO: Do we really need these??
        //public virtual TEntity Update(TPrimaryKey id, Action<TEntity> updateAction)
        //{
        //    var entity = Get(id);
        //    updateAction(entity);
        //    return entity;
        //}

        //public virtual async Task<TEntity> UpdateAsync(TPrimaryKey id, Func<TEntity, Task> updateAction)
        //{
        //    var entity = await GetAsync(id);
        //    await updateAction(entity);
        //    return entity;
        //}

        #endregion

        #region Save Changes

        public void SaveChanges()
        {
            Context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await Context.SaveChangesAsync();
        }

        public async Task SaveChangesAsync(CancellationToken cancellationToken)
        {
            await Context.SaveChangesAsync(cancellationToken);
        }

        #endregion

        #region Delete

        public virtual void Delete(TEntity entity)
        {
            AttachIfNot(entity);
            Table.Remove(entity);
        }

        public virtual Task DeleteAsync(TEntity entity)
        {
            Delete(entity);
            return Task.FromResult(0);
        }

        public virtual void Delete(TPrimaryKey id)
        {
            var entity = Table.Local.FirstOrDefault(ent => EqualityComparer<TPrimaryKey>.Default.Equals(ent.Id, id));
            if (entity == null)
            {
                entity = FirstOrDefault(id);
                if (entity == null)
                {
                    return;
                }
            }

            Delete(entity);
        }

        public virtual Task DeleteAsync(TPrimaryKey id)
        {
            Delete(id);
            return Task.FromResult(0);
        }

        public virtual void Delete(Expression<Func<TEntity, bool>> predicate)
        {
            foreach (var entity in GetAll().Where(predicate).ToList())
            {
                Delete(entity);
            }
        }

        public virtual Task DeleteAsync(Expression<Func<TEntity, bool>> predicate)
        {
            Delete(predicate);
            return Task.FromResult(0);
        }

        #endregion

        #region Aggregates

        public virtual int Count()
        {
            return GetAll().Count();
        }

        public virtual Task<int> CountAsync()
        {
            return Task.FromResult(Count());
        }

        public virtual int Count(Expression<Func<TEntity, bool>> predicate)
        {
            var query = GetAll();

            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            return query.Count();
        }

        public virtual Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return Task.FromResult(Count(predicate));
        }

        public virtual long LongCount()
        {
            return GetAll().LongCount();
        }

        public virtual Task<long> LongCountAsync()
        {
            return Task.FromResult(LongCount());
        }

        public virtual long LongCount(Expression<Func<TEntity, bool>> predicate)
        {
            var query = GetAll();

            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            return query.LongCount();
        }

        public virtual Task<long> LongCountAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return Task.FromResult(LongCount(predicate));
        }


        #endregion

        #region Protected Methods

        protected virtual IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> predicate = null)
        {
            var query = Table != null ? Table.AsQueryable<TEntity>() : null;// Querable.AsQueryable();

            // Set the predicate for query on where clause
            if (predicate != null)
                query = Table.Where(predicate);

            return query;
        }

        protected virtual bool IsTransient(TEntity entity)
        {
            if (EqualityComparer<TPrimaryKey>.Default.Equals(entity.Id, default(TPrimaryKey)))
            {
                return true;
            }

            //Workaround for EF Core since it sets int/long to min value when attaching to dbcontext
            if (typeof(TPrimaryKey) == typeof(int))
            {
                return Convert.ToInt32(entity.Id) <= 0;
            }

            if (typeof(TPrimaryKey) == typeof(long))
            {
                return Convert.ToInt64(entity.Id) <= 0;
            }

            if (typeof(TPrimaryKey) == typeof(string))
            {
                return string.IsNullOrWhiteSpace(entity.Id.ToString()) ? false : true;
            }

            return false;
        }

        protected virtual void AttachIfNot(TEntity entity)
        {
            if (!Table.Local.Contains(entity))
            {
                Table.Attach(entity);
            }
        }

        protected virtual Expression<Func<TEntity, bool>> CreateEqualityExpressionForId(TPrimaryKey id)
        {
            var lambdaParam = Expression.Parameter(typeof(TEntity));

            var lambdaBody = Expression.Equal(
                Expression.PropertyOrField(lambdaParam, "Id"),
                Expression.Constant(id, typeof(TPrimaryKey))
                );

            return Expression.Lambda<Func<TEntity, bool>>(lambdaBody, lambdaParam);
        }

        public Task<List<TEntity>> GetAllListAsync(Expression<Func<TEntity, bool>> predicate, List<Expression<Func<TEntity, object>>> propertySelectors = null)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<TEntity>> GetPagedList(int page, int pagesize, string sortOrder, string sortField, Expression<Func<TEntity, bool>> predicate = null, List<Expression<Func<TEntity, object>>> includes = null, IQueryable<TEntity> query = null)
        {
            throw new NotImplementedException();
        }

        public Task<List<TEntity>> GetAllListWithTrackingAsync(Expression<Func<TEntity, bool>> predicate, List<Expression<Func<TEntity, object>>> includes = null)
        {
            throw new NotImplementedException();
        }
    }

    #endregion Protected Methods
}
