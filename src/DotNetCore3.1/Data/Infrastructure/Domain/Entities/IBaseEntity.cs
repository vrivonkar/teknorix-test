﻿namespace TeknorixTest.Data.Infrastructure.Domain.Entities
{
    public interface IBaseEntity : IEntity
    {
    }
    public interface IBaseEntity<TPrimaryKey> : IEntity<TPrimaryKey>
    {
    }
}
