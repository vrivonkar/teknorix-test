﻿using Microsoft.EntityFrameworkCore;

namespace TeknorixTest.Data.Infrastructure
{
    public class DbContextProvider
       : IDbContextProvider
    {
        private DbContext _dbContext { get; }

        public DbContextProvider(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public DbContext GetDbContext()
        {
            return _dbContext;
        }
    }

    public class DbContextProvider<TDBContext>
        : IDbContextProvider<TDBContext> where TDBContext : DbContext
    {
        private TDBContext _dbContext { get; }

        public DbContextProvider(TDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public TDBContext GetDbContext()
        {
            return _dbContext;
        }
    }
}
