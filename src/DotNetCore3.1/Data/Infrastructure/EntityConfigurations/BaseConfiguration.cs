﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TeknorixTest.Data.Infrastructure.EntityConfigurations
{
    public abstract class BaseConfiguration<T> : IEntityTypeConfiguration<T> where T : class
    {
        protected EntityTypeBuilder<T> Builder;
        public void Configure(EntityTypeBuilder<T> builder)
        {
            Builder = builder;
            ToTable();
            SetPrimaryKey();
            SetForeignKey();
            SetProperties();
        }
        public virtual void ToTable()
        {
            Builder.ToTable(typeof(T).Name);

        }
        public virtual void SetPrimaryKey() { }
        public virtual void SetForeignKey() { }
        public virtual void SetProperties() { }
    }
}
