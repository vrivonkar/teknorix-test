﻿using Microsoft.EntityFrameworkCore;

namespace TeknorixTest.Data.Infrastructure
{
    public interface IDbContextProvider : IDbContextProvider<DbContext>
    {
    }

    public interface IDbContextProvider<TDBContext> where TDBContext : DbContext
    {
        TDBContext GetDbContext();
    }
}
