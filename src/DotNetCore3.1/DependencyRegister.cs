﻿using TeknorixTest.Data;
using TeknorixTest.Data.Infrastructure;
using TeknorixTest.Data.Infrastructure.Domain;
using TeknorixTest.Services;
using TeknorixTest.Services.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace DotNetCore_3_1
{
    public static class DependencyRegister
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<DbContext, TestDbContext>();
            services.AddScoped<IDbContextProvider<TestDbContext>, DbContextProvider<TestDbContext>>();
            services.AddScoped<IDbContextProvider, DbContextProvider>();
            // Register Generic repositories
            services.AddScoped(typeof(IRepository<>), typeof(GenericRepository<>));
            services.AddScoped(typeof(IRepository<,>), typeof(GenericRepository<,>));
            //Register application services
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IUserService, UserService>();
            services.AddSingleton<ILocalAuthenticationService, CookieAuthenticationService>();
        }
    }
}
