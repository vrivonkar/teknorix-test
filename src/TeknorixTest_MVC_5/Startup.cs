﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TeknorixTest_MVC_5.Startup))]
namespace TeknorixTest_MVC_5
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
