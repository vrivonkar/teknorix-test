﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeknorixTest.Data;

namespace TeknorixTest_MVC_5.Data.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        TestDbContext Init();
    }
}