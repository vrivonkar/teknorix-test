﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeknorixTest_MVC_5.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}