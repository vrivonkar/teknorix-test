﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeknorixTest.Data;

namespace TeknorixTest_MVC_5.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        TestDbContext dbContext;

        public TestDbContext Init()
        {
            return dbContext ?? (dbContext = new TestDbContext());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}