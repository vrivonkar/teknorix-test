﻿using TeknorixTest.Data.Domain;
using TeknorixTest.Data.Infrastructure.EntityConfigurations;

namespace TeknorixTest.Data.EntityConfigurations
{
    public class UserConfiguration : BaseConfiguration<User>
    {

        public override void SetPrimaryKey()
        {
            HasKey(r => r.Id);

        }
        public override void SetProperties()
        {
            Property(r => r.Id).IsRequired();
            Property(r => r.Email).HasMaxLength(500).IsRequired();
            Property(r => r.FirstName).HasMaxLength(500).IsRequired();
            Property(r => r.IsActive).IsRequired();
        }
        public override void SetForeignKey()
        {
            base.SetForeignKey();
        }
    }
}
