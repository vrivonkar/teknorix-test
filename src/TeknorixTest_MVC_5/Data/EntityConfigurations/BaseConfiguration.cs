﻿
using System.Data.Entity.ModelConfiguration;

namespace TeknorixTest.Data.Infrastructure.EntityConfigurations
{
    public abstract class BaseConfiguration<T> : EntityTypeConfiguration<T> where T : class
    {
        public BaseConfiguration()
        {
            ToTable(this.GetType().Name);
            SetPrimaryKey();
            SetForeignKey();
            SetProperties();
        }


        public virtual void SetPrimaryKey() { }
        public virtual void SetForeignKey() { }
        public virtual void SetProperties() { }
    }
}
