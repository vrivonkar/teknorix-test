﻿using System;

namespace TeknorixTest.Data.Infrastructure.Domain.Entities
{
    public abstract class BaseEntity : IBaseEntity, IEntity
    {
        public int Id { get; set; }

        public virtual DateTime CreatedAt { get; set; }

        public virtual DateTime? ModifiedAt { get; set; }

    }

    public abstract class BaseEntity<TPrimaryKey> : IBaseEntity<TPrimaryKey>
    {
        public TPrimaryKey Id { get; set; }

    }
}
