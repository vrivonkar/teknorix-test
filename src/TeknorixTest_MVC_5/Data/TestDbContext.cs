﻿using TeknorixTest.Data.EntityConfigurations;

using System.Data.Entity;

namespace TeknorixTest.Data
{
    public class TestDbContext : DbContext
    {
        public TestDbContext() : base("DefaultConnection") { }
        public virtual void Commit()
        {
            base.SaveChanges();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserConfiguration());
        }

    }
}
