﻿using TeknorixTest.Data;
using TeknorixTest.Data.Infrastructure;
using TeknorixTest.Data.Infrastructure.Domain;
using TeknorixTest.Services;

using Autofac;
using System.Data.Entity;
using TeknorixTest_MVC_5.Data.Infrastructure;
using TeknorixTest.Data.Domain;

namespace MVCTest
{
    public static class DependencyRegister
    {
        public static void RegisterServices(this ContainerBuilder builder)
        {
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterType<DbFactory>().As<IDbFactory>().InstancePerRequest();
            builder.RegisterType(typeof(Repository<User>)).As(typeof(IRepository<User>)).InstancePerRequest();
            builder.RegisterType<UserService>().As<IUserService>().InstancePerRequest();

        }
    }
}
