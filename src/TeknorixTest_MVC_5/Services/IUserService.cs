﻿using TeknorixTest.Models;
using TeknorixTest_MVC_5.Models;

namespace TeknorixTest.Services
{
    public interface IUserService
    {
        bool ValidateUser(LoginViewModel model);
    }
}